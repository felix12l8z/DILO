﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{
    //Rigidbody 2D bola
    private Rigidbody2D rigidBody2D;

    //besar gagaya awal ke bola
    public float xInitialForce;
    public float yInitialForce;
    //Titik asal lintasan bola
    private Vector2 trajectoryOrigin;
    void ResetBall()
    {
        //Reset position(0,0)
        transform.position = Vector2.zero;
        //Reset speed
        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall()
    {
        //nilai komponen y dari gaya dorong InitialForce
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);
        //random number 0(inklusi) - 2 (ekslusif)
        float randomDirection = Random.Range(0,2);

        //value < 1 ke kiri
        //value > 1 ke kanan
        if (randomDirection < 1.0f)
        {
                //Gaya untuk menggerakkan bola
                rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));
                
        }
        else
        {
            rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce));
        }
    }
    void RestartGame()
    {
        //Reset posisi bola
        ResetBall();
        //setiap 2 detik berikan gaya dorong ke bola
        Invoke("PushBall", 2);
    }

    //rekam titik tumbukan terakhir
    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }
    //mengakses trajectory origin dari luar
    public Vector2 TrajectoryOrigin
    {
        get {return trajectoryOrigin;}
    }
    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        //start game
        RestartGame();
        trajectoryOrigin = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
