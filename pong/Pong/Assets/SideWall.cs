﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour
{
    //Pemain yang akan mendapatkan score saat melewati dinding
    public PlayerControl player;
    // Skrip GameManager untuk mengakses skor maksimal
    [SerializeField]
    private GameManager gameManager;
    void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        //jika objek bernama "Ball":
        if(anotherCollider.name == "Ball")
        {
            //Tambahkan score
            player.IncrementScore();

            //jika pemain score nya belum maksimal
            if(player.Score < gameManager.maxScore)
            {
                //restart game setelah bola mengenai dinding
                anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
