﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    //Script, collider, rigidbody  bola
    public BallControl ball;
    CircleCollider2D ballCollider;
    Rigidbody2D ballRigidbody;

    //Bola "Bayangan" yang akan ditampilkan di titik tumbukan
    public GameObject ballAtCollision;
    
    // Start is called before the first frame update
    void Start()
    {
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Inisiasi status pantulan linstasan, yang akan ditmapilkan jika lintasan bertumbukan dengan objek tertentu.
        bool drawBallAtCollision = false;
        //TItik tumbukan yang digeser, untuk menggambar ballAtCollision
        Vector2 offsetHitPoint = new Vector2();
        //Tentukan titik tumbukan dengan deteksi pergerakan lingkaran
        RaycastHit2D[] circleCastHit2DArray = Physics2D.CircleCastAll(ballRigidbody.position, ballCollider.radius, ballRigidbody.velocity.normalized);

        // Untuk setiap titik tumbukan
        foreach( RaycastHit2D circleCastHit2D in circleCastHit2DArray)
        {
            //Jika terjadi tumbukan dan tumbukan tersebut tidak dengan bola
            if (circleCastHit2D.collider != null && 
                circleCastHit2D.collider.GetComponent<BallControl>() == null)
                {
                    //Garisi lintasan digambarkan dari titik tengah bola saat ini ke titik tengah bola saat tumbukan terjadi
                    //yaitu sebuah titik yang di offset dari titik tumbukan berdasar vektor normal titik terbesar
                    //jari-jari bola
                    // tentukan titik tumbukan
                    Vector2 hitPoint = circleCastHit2D.point;

                    //tentukan normal di tiitk tumbukan 
                    Vector2 hitNormal = circleCastHit2D.normal;

                    //tentukan offsetHitPoint, yaitu titik tengah bola pada saat bertumbukan
                    offsetHitPoint = hitPoint + hitNormal * ballCollider.radius;
                    // Gambar garis lintasan dari titik tengah bola saat ini ke titik tengah bola pada saat bertumbukan
                    DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);
                    
                    //kalau bukan sidewall, gambar pantulannya
                    if (circleCastHit2D.collider.GetComponent<SideWall>() == null)
                    {
                        //Hitung vektor datang
                        Vector2 inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;

                        //Hitung vektor keluar
                        Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

                        //hitung dot product dari outVector dari hitNormal. digunakan supaya ketika garis tumbukan terjadi tumbukan tidak digambar
                        float outDot = Vector2.Dot(outVector, hitNormal);
                        if (outDot > -1.0f && outDot < 1.0)
                        {
                            //Gambar lintasan pantulannya
                            DottedLine.DottedLine.Instance.DrawDottedLine(
                                offsetHitPoint,
                                offsetHitPoint + outVector * 10.0f);

                            //untuk menggambar "bola" bayangan di prediksi titik tumbukan
                            drawBallAtCollision = true;
                        }
                    }
                //Hanya gambar lintasan untuk satu titik tumbukan, jadi keluar dari loop
                break;
                }
        }
        //Jika True
        if (drawBallAtCollision)
        {
            //Gambar bola "bayangan" di prediksi titik tumbukna
            ballAtCollision.transform.position = offsetHitPoint;
            ballAtCollision.SetActive(true);
        }else
        {
            //sembunyikan bola "bayangan"
            ballAtCollision.SetActive(false);
        }
        
    }
}
