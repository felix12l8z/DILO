﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PlayerControl player1; //script
    private Rigidbody2D player1Rigidbody;
    public PlayerControl player2; //script
    private Rigidbody2D player2Rigidbody;
    //Ball
    public BallControl ball; //script
    private Rigidbody2D ballRigidbody;
    private CircleCollider2D ballCollider;
    //maximum score
    public int maxScore;
    //tombol debug
    
    public Trajectory trajectory;
    // Start is called before the first frame update
    void Start()
    {
        player1Rigidbody = player1.GetComponent<Rigidbody2D>();
        player2Rigidbody = player2.GetComponent<Rigidbody2D>();
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    //Untuk menampilkan GUI
    void onGUI()
    {
        trajectory.enabled = !trajectory.enabled;
        //menampilkan score pemain diatas nama
        GUI.Label(new Rect(Screen.width / 2 - 150 - 12, 20, 100, 100), "" + player1.Score);
        GUI.Label(new Rect(Screen.width / 2 + 150 + 12, 20, 100, 100), "" + player2.Score);
        //Button restart
        if (GUI.Button(new Rect(Screen.width / 2 - 60, 35, 120, 53), "RESTART"))
        {
            //tombol restart ditekan, reset semua score pemain
            player1.ResetScore();
            player2.ResetScore();

            //restart game
            ball.SendMessage("RestartGame", 0.5f, SendMessageOptions.RequireReceiver);
        }
            //jika player 1 menang
        if (player1.Score == maxScore)
        {
            //tampilkan teks di kiri layar
            GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 - 10, 2000, 1000), "PLAYER ONE WINS");

            //kembalikan bola ke tengah
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);
        }

        //jika player 2 menang
        else if (player2.Score == maxScore)
        {
            //tampilkan teks di kanan layar
            GUI.Label(new Rect(Screen.width / 2 + 30, Screen.height / 2 - 10, 2000, 1000), "PLAYER TWO WINS");

            //kembalikan bola ke tengah
            ball.SendMessage("ResetBall", null, SendMessageOptions.RequireReceiver);

        }
            //jika isDebugWindowShown == true maka tampilkan text area untuk debug window
        if (isDebubgWindowShown == true)
    {
            //simpan nilai warna lama GUI
            Color oldColor = GUI.backgroundColor;
            //warna background debug window
            GUI.backgroundColor = Color.red;
            //Variabel fisika yang akan ditampilkan
            float ballMass = ballRigidbody.mass;
            Vector2 ballVelocity = ballRigidbody.velocity;
            float ballSpeed = ballRigidbody.velocity.magnitude;
            Vector2 ballMomentum = ballMass * ballVelocity;
            float ballFriction = ballCollider.friction;

            float impulsePlayer1X = player1.LastContactPoint.normalImpulse;
            float impulsePlayer1Y = player1.LastContactPoint.tangentImpulse;
            float impulsePlayer2X = player2.LastContactPoint.normalImpulse;
            float impulsePlayer2Y = player2.LastContactPoint.tangentImpulse;

            //Debug Text speed momentum friction
            string debugText = 
                "Ball mass = " + ballMass + "\n" +
                "Ball velocity = " + ballVelocity + "\n" + 
                "Ball speed = " + ballSpeed + "\n" +
                "Ball momentum = " + ballMomentum + "\n" +
                "Ball friction = " + ballFriction + "\n" +
                "Last impulse from player 1 = (" + impulsePlayer1X + ", " + impulsePlayer1Y + ")\n" +
                "Last impulse from player 2 = (" + impulsePlayer2X + ", " + impulsePlayer2Y + ")\n";
            
            //Menampilkan debug window
            GUIStyle guiStyle = new GUIStyle(GUI.skin.textArea);
            guiStyle.alignment = TextAnchor.UpperCenter;
            GUI.TextArea(new Rect(Screen.width/2 - 200, Screen.height - 200, 400, 110), debugText, guiStyle);
            //kembalikan warna GUI lama
            GUI.backgroundColor = oldColor;
            if (GUI.Button(new Rect(Screen.width/2 - 60, Screen.height - 73, 120, 53), "TOGGLE\nDEBUG NFO"))
            {
                isDebubgWindowShown = !isDebubgWindowShown;
            } 
    } 
    }
    private bool isDebubgWindowShown = false;   
    // Update is called once per frame
    void Update()
    {
        
    }
}
