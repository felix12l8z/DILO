﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public KeyCode upButton = KeyCode.W;
    public KeyCode downButton = KeyCode.S;

    // Kecepatan Gerak
    public float speed = 10.0f;

    //Batas atas dan bawah game scene (Batas bawah menggunakan minus(-))
    public float yBoundary = 9.0f;

    //Rigidbody 2d raket
    private Rigidbody2D rigidBody2D;

    //Score
    private int score;
    //Titik tumbukan terakhir dengan bola
    private ContactPoint2D lastContactPoint;

    // Start is called before the first frame update
    public ContactPoint2D LastContactPoint
    {
        get{ return lastContactPoint;}
    }

    //ketika terjadi tumbukan dengan bola, masukka titik kontaknya ke log
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name.Equals("Ball"))
        {
            lastContactPoint = collision.GetContact(0);
        }
    }

    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //Raket speed
        Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
        Vector3 position = transform.position;

        if(Input.GetKey(upButton))
        {
            velocity.y = speed;
        }
        else if(Input.GetKey(downButton))
        {
            velocity.y = -speed;
        }
        else
        {
            velocity.y = 0.0f;
        }
        //update value velocity untuk posisi rigidbody2d
        GetComponent<Rigidbody2D>().velocity = velocity;

        //batas atas
        if(position.y > yBoundary)
        {
            position.y = yBoundary;
        }
        //batas bawah
        else if(position.y < -yBoundary)
        {
            position.y = -yBoundary;
        }

        //update value transform position
        transform.position = position;
    }
    //menaikkan skor 1 poin
    public void IncrementScore()
    {
        score++;
    }
    //reset score
    public void ResetScore()
    {
        score = 0;
    }
    //mengupdate nilai score
    public int Score
    {
        get {return score;}
    }
}
